import sys
import mp2
import opponent_black
import test

BLACK, WHITE = "B","W"
def check_end_game(color, filename):
	f = open(filename)
	data = []
	for line in f.readlines():
		data.append(line)
	TIME = float(data[2].strip())
	BOARD = [list(i.strip()) for i in data[3:]]
	BLACK, WHITE = "B","W"
	
	WHITETAR = [[0,0], [1,0], [2,0], [3,0], [4,0],
				[0,1], [1,1], [2,1], [3,1], [4,1], 
				[0,2], [1,2], [2,2], [3,2],
				[0,3], [1,3], [2,3],
				[0,4], [1,4]]
	BLACKTAR = [                           [14,11], [15,11],
						 		  [13,12], [14,12], [15,12],
				         [12,13], [13,13], [14,13], [15,13],
				[11,14], [12,14], [13,14], [14,14], [15,14],
				[11,15], [12,15], [13,15], [14,15], [15,15]]
	
	# color = WHITE if filename=="output.txt" else BLACK
	tarcamp = WHITETAR if color == WHITE else BLACKTAR
	if all(BOARD[y][x] == color for x,y in tarcamp):
		return True
	else:
		return False
	

# def main():
# 	test.generate_original_board()
# 	for i in range(200):
# 		mp2.main()
# 		if check_end_game(WHITE, "output.txt"):
# 			print "white wins"
# 			break
# 		opponent_black.main()
# 		if check_end_game(BLACK, "input.txt"):
# 			print "black wins"
# 			break
# 		print "      ", i, " moves"

def main():
	test.generate_original_board()
	for i in range(200):
		opponent_black.main()
		if check_end_game(BLACK, "input.txt"):
			print "black wins"
			break
		print "      ", i, " moves"
		mp2.main()
		if check_end_game(WHITE, "output.txt"):
			print "white wins"
			break
		

if __name__ == '__main__':
	main()


