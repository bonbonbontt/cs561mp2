#!/bin/bash
# python test.py ori

for i in {1..1}
do 
	python mp2.py
	let end=$(python game.py output.txt 2>&1)
	echo $end
	if [ $end -eq 1 ]; then
		echo "white wins"
		break
	fi
	python opponent-black.py
	let end2=$(python game.py input.txt)
	if [ $end2 -eq 1 ]; then
		echo "black wins"
		break
	fi
done            