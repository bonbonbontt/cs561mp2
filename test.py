from random import randrange
import random
import time
import sys

def generate_random_board(seed):
	

	random.seed(seed)
	board = [['.' for i in range(16)]for j in range(16)]
	count = 19
	while count>0:
		x,y = randrange(16),randrange(16)
		if board[y][x] == '.':
			board[y][x] = "W"
			count -= 1
	
	count = 19
	while count>0:
		x,y = randrange(16),randrange(16)
		if board[y][x] == '.':
			board[y][x] = "B"
			count -= 1
	
	# for i in board:
	# 	print i
	return board

def generate_original_board():
	blacks = [[0,0], [1,0], [2,0], [3,0], [4,0],
			[0,1], [1,1], [2,1], [3,1], [4,1], 
			[0,2], [1,2], [2,2], [3,2],
			[0,3], [1,3], [2,3],
			[0,4], [1,4]]
	whites = [                       [14,11], [15,11],
					 		  [13,12], [14,12], [15,12],
			         [12,13], [13,13], [14,13], [15,13],
			[11,14], [12,14], [13,14], [14,14], [15,14],
			[11,15], [12,15], [13,15], [14,15], [15,15]]

	board = [['.' for i in range(16)]for j in range(16)]
	for x,y in blacks:
		board[y][x] = "B"
	for x,y in whites:
		board[y][x] = "W"
	f = open("output.txt", "w")
	f.write("GAME\nBLACK\n300.0\n")
	count = 0

	for line in board:
			f.write("".join(line))
			count+=1
			if count != 16:
				f.write("\n")
	f.close()
	for line in board:
		print line




def main():
	mode = sys.argv[1]
	if mode == "random":
		for i in range(11, 21):
			board = generate_random_board(i)
			f = open("input%d.txt"%i, "w")
			f.write("SINGLE\nBLACK\n23.33\n")
			count = 0
			for line in board:
				f.write("".join(line))
				count+=1
				if count != 16:
					f.write("\n")
		
			f.close()
		print "random input11-20 done"
	else:
		generate_original_board()
		print "new game board done"

if __name__ == '__main__':
	main()

