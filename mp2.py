# todo:
# game agent with time limit

# add time limit to game mode

# ab ordering
# read instructions & test cases
import math
global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
import time

SINGLE,GAME = 0,1
BLACK, WHITE = "B","W"
neighbors = [[0,1], [0,-1],[1,1], [1, -1], [1, 0], [-1, 0], [-1, 1], [-1,-1]]
WIDTH=16
WHITETAR = [[0,0], [1,0], [2,0], [3,0], [4,0],
			[0,1], [1,1], [2,1], [3,1], [4,1], 
			[0,2], [1,2], [2,2], [3,2],
			[0,3], [1,3], [2,3],
			[0,4], [1,4]]
BLACKTAR = [                           [14,11], [15,11],
					 		  [13,12], [14,12], [15,12],
			         [12,13], [13,13], [14,13], [15,13],
			[11,14], [12,14], [13,14], [14,14], [15,14],
			[11,15], [12,15], [13,15], [14,15], [15,15]]

GAME_TEST = 1
DEBUG = 1






# for line in WEVALMAP:
# 	print line
# for line in BEVALMAP:
# 	print line


# find legal moves for a given piece
def find_legal_moves(board, x, y):
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	# print "   ",x,y,"   "
	legal_moves = []
	visited = []

	# no jump
	for p, q in neighbors:
		newx, newy = x+p, y+q
		if newx>=0 and newy>=0 and newx<WIDTH and newy<WIDTH and board[newy][newx]=='.':
			legal_moves.append([x, y, newx, newy])

	#jumps
	jumps = [[x,y]] #[x, y, x1, y1]
	while len(jumps)>0:
		cur = jumps.pop()
		cx, cy = cur[-2],cur[-1]
		for p, q in neighbors:
			newx, newy = cx+p, cy+q
			# jumped to visited loc
			if any((cx+p*2==i[0] and cy+q*2==i[1]) for i in visited):
				continue
			# jumped to unvisited loc
			if newx>=0 and newy>=0 and newx<WIDTH and newy<WIDTH and board[newy][newx]!='.':
				newx, newy = cx+p*2, cy+q*2
				if newx>=0 and newy>=0 and newx<WIDTH and newy<WIDTH and board[newy][newx]=='.':
					new_jump = cur + [newx, newy]
					visited.append([newx, newy])
					jumps.append(new_jump) 
					legal_moves.append(new_jump)
	return legal_moves

def find_all_legal_moves(board, color):
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	camp = BLACKTAR if color == WHITE else WHITETAR
	incamp = any((board[i[1]][i[0]]==color) for i in camp)
	tarcamp = WHITETAR if color == WHITE else BLACKTAR
	outcamp = []
	further = []
	legal_moves = []

	if incamp: # find all incamp pieces
		incamp_pieces = []
		for x,y in camp:
			if board[y][x] == color:
				incamp_pieces.append([x,y])
		# find can move out pieces and move further pieces
		for piece in incamp_pieces:
			moves = find_legal_moves(board, piece[0], piece[1])
			for move in moves:
				if all((i[0]!=move[-2] or i[1]!=move[-1]) for i in camp):
					outcamp.append(move)
				elif (color==BLACK and move[-2]>=move[0] and move[-1]>=move[1]) or (color==WHITE and move[-2]<=move[0] and move[-1]<=move[1]):
					further.append(move)
				else:
					pass
	# move out of camp
	if len(outcamp)!=0:
		return outcamp
	# move further
	elif len(further)!=0:
		return further
	# can't move out or further, consider outcamp pieces
	else:
		# find out camp pieces
		my_pieces = []
		for x in range(WIDTH):
			for y in range(WIDTH):
				if (not any((x==i[0] and y==i[1]) for i in camp)) and BOARD[y][x] == COLOR:
					my_pieces.append([x,y])
		for piece in my_pieces:
			moves = find_legal_moves(board, piece[0], piece[1])
			for move in moves:
				# move from tarcamp to outcamp
				if any((move[0]==i[0] and move[1]==i[1])for i in tarcamp) and not any((move[-2]==j[0] and move[-1]==j[1])for j in tarcamp):
					continue
				else:
					legal_moves.append(move)
	# print legal_moves
	return legal_moves

def eval(state, color):
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	white = 0.0
	black = 0.0
	for x in range(WIDTH):
		for y in range(WIDTH):
			if state[y][x] == WHITE:
				# incamp = any((i[0]==x and i[1]==y) for i in WHITETAR)
				# d = math.sqrt((0-x)**2+(0-y)**2)
				# a = 10.0 if d == 0.0 else 1/d
				white += WEVALMAP[y][x]
			if state[y][x] == BLACK:
				# incamp = any((i[0]==x and i[1]==y) for i in BLACKTAR)
				# d = math.sqrt((15-x)**2+(15-y)**2)
				# a = 10.0 if d == 0.0 else 1/d
				black += BEVALMAP[y][x]

	return 6000-(white-black) if COLOR == WHITE else 6000-(black-white)


def end_game(board, color):
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	tarcamp = WHITETAR if color == WHITE else BLACKTAR
	return all(board[y][x] == color for x,y in tarcamp)

def minimax(state, depth, color, alpha, beta):
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	best = [-1, -1, -1,-1,-float("inf")] if color == COLOR else [-1, -1, -1,-1,float("inf")]
	opponent = WHITE if color == BLACK else BLACK
	if depth == 0 or end_game(state, opponent):
		score = eval(state, COLOR)
		return [-1,-1,-1,-1,score]

	all_legal_moves = find_all_legal_moves(state, color)

	# generate new states
	for move in all_legal_moves:
		# vitual move
		piece = state[move[1]][move[0]]
		state[move[1]][move[0]] = '.'
		state[move[-1]][move[-2]] = piece
		# evaluate
		score = minimax(state, depth-1, opponent, alpha, beta)
		# put vitual moves back
		state[move[1]][move[0]] = piece
		state[move[-1]][move[-2]] = '.'
		score[0:-1] = move

		# alpha-beta pruning
		if color == COLOR:
			if score[-1] > alpha:
				alpha = score[-1]
				best = score # max
			if alpha >= beta:
				break
		else:
			if score[-1] < beta:
				beta = score[-1]
				best = score # min
			if alpha >= beta:
				break
	
	return best

def eval_maps(color):
	target = [0,0] if color==WHITE else [15, 15]
	tarcamp = WHITETAR if color==WHITE else BLACKTAR
	eval_map = [[0.0 for i in range(16)] for j in range(16)]
	for x in range(WIDTH):
		for y in range(WIDTH):
			if x==target[0] and y==target[1]:
				eval_map[y][x] = 0
			else:
				# eval_map[y][x] = 1/(float(abs(target[0]-x))+float(abs(target[1]-y)))
				eval_map[y][x] = round(math.sqrt((target[0]-x)**2+(target[1]-y)**2),2)
				# if any((x==i[0] and y==i[1]) for i in tarcamp):
				# 	eval_map[y][x] += 0.1
	# for line in eval_map:
	# 	print line
	# print "-"*10
	return eval_map

def main():
	start = time.clock()
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	# read file
	f = open("input.txt")
	data = []
	for line in f.readlines():
		data.append(line)
	MODE = SINGLE if data[0].strip()=="SINGLE" else GAME
	COLOR = BLACK if data[1].strip()=="BLACK" else WHITE
	TIME = float(data[2].strip())
	BOARD = [list(i.strip()) for i in data[3:]]
	
	
	
	WEVALMAP = eval_maps(WHITE)
	BEVALMAP = eval_maps(BLACK)
	# for i in range(7):
	# 	print WEVALMAP[i][0:7]

	# TODO: time exceed
	# single step
	if MODE == SINGLE:
		depth = 1
		move = minimax(BOARD, depth, COLOR, -float("inf"), float("inf"))
	
		# output
		f = open("output.txt", "w")
		if len(move)>5 or abs(move[0]-move[2])>1 or abs(move[1]-move[3])>1: 
			#jump
			for i in range(0,len(move)-3, 2):
				ret = "J %d,%d %d,%d" % (move[0+i], move[1+i], move[2+i], move[3+i])
				if DEBUG:
					print ret
				f.write(ret)
				if i+2<len(move)-3:
					f.write("\n")
		else:
			#walk
			if DEBUG:
				print "E %d,%d %d,%d" % (move[0], move[1], move[2], move[3])
			f.write("E %d,%d %d,%d" % (move[0], move[1], move[2], move[3]))
	
	
		# make the move
		piece = BOARD[move[1]][move[0]]
		BOARD[move[1]][move[0]] = '.'
		BOARD[move[-2]][move[-3]] = piece
		if DEBUG:
			for line in BOARD:
				print line
	
	

	# game agent
	if MODE == GAME:
		# find not satisfied one
		tarcamp = WHITETAR if COLOR==WHITE else BLACKTAR
		empty = []
		for x,y in WHITETAR:
			if BOARD[y][x] != COLOR:
				empty.append([x,y])
		s = []
		if len(empty) == 1:
			# find not good piece
			t = empty[0]
			# print "t", t
			# print BOARD
			for x in range(15):
				for y in range(15):
					if BOARD[y][x]==COLOR and (not any((x==i[0] and y==i[1]) for i in tarcamp)):
						s = [x,y]
						break
			# print "s", s
			legal_moves = find_legal_moves(BOARD, s[0], s[1])
			best_move = 0
			best_val = 32
			for move in legal_moves:

				d = abs(move[-2]-t[0])+abs(move[-1]-t[1])
				# print move, d
				if d < best_val:
					best_val = d
					best_move = move
			move = best_move + [0]
			if DEBUG:
				print move

		else:
			
			if TIME > 100:
				depth = 1
			else: 
				depth = 1
			move = minimax(BOARD, depth, COLOR, -float("inf"), float("inf"))
			if DEBUG:
				print move
	
		# output
		f = open("output.txt", "w")
		if len(move)>5 or abs(move[0]-move[2])>1 or abs(move[1]-move[3])>1: 
			#jump
			for i in range(0,len(move)-3, 2):
				ret = "J %d,%d %d,%d" % (move[0+i], move[1+i], move[2+i], move[3+i])
				if DEBUG:
					print ret
				f.write(ret)
				if i+2<len(move)-3:
					f.write("\n")
		else:
			#walk
			if DEBUG:
				print "E %d,%d %d,%d" % (move[0], move[1], move[2], move[3])
			f.write("E %d,%d %d,%d" % (move[0], move[1], move[2], move[3]))
		f.close()
	
	
		# make the move
		piece = BOARD[move[1]][move[0]]
		BOARD[move[1]][move[0]] = '.'
		BOARD[move[-2]][move[-3]] = piece
		if DEBUG:
			for line in BOARD:
				print line

		end = time.clock()
		elapse = end-start
		realtime = TIME - elapse

		if GAME_TEST:
			f = open("output.txt", "w")
			f.write("GAME\n")
			f.write("BLACK\n")
			f.write(str(realtime)+"\n")
			count = 0
			for line in BOARD:
				f.write("".join(line))
				count+=1
				if count != 16:
					f.write("\n")
			f.close()

if __name__ == '__main__':
	main()
	
	


