from random import randrange
WHITETAR = [[0,0], [1,0], [2,0], [3,0], [4,0],
			[0,1], [1,1], [2,1], [3,1], [4,1], 
			[0,2], [1,2], [2,2], [3,2],
			[0,3], [1,3], [2,3],
			[0,4], [1,4]]
BLACKTAR = [                           [14,11], [15,11],
					 		  [13,12], [14,12], [15,12],
			         [12,13], [13,13], [14,13], [15,13],
			[11,14], [12,14], [13,14], [14,14], [15,14],
			[11,15], [12,15], [13,15], [14,15], [15,15]]

BLACK, WHITE = "B","W"

def test_incamp_out(color):
	camp = WHITETAR if color==BLACK else BLACKTAR
	oppo_camp = BLACKTAR if color==BLACK else WHITETAR
	oppo_color = BLACK if color==WHITE else WHITE
	for index in range(len(camp)):
		cx, cy = camp[index]
		board = [['.' for i in range(16)] for j in range(16)]
		board[cy][cx] = color
		count = 19
		while count>0:
			x,y = randrange(16),randrange(16)
				
			if board[y][x] == '.' and (x!=cx and y!=cy) and (not any((x==i[0] and y==i[1]) for i in camp)):
				board[y][x] = color
				count -= 1
			
		for cx, cy in oppo_camp:
			board[cy][cx] = oppo_color

		# output
		f = open("test_incamp_out_%s/input%d.txt"%(color,index), "w")
		player = "WHITE" if color==WHITE else "BLACK"
		f.write("GAME\n%s\n300.0\n"%player)
		count = 0
		for line in board:
				f.write("".join(line))
				count+=1
				if count != 16:
					f.write("\n")
		f.close()






if __name__ == '__main__':
	test_incamp_out(BLACK)
	test_incamp_out(WHITE)



	
	