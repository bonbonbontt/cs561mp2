# todo:
# game agent
# game agent with time limit
# if all in tar camp, can move tar camp pieces
# add time limit to game mode
# alpha beta pruning
SINGLE,GAME = 0,1
BLACK, WHITE = "B","W"
neighbors = [[0,1], [0,-1],[1,1], [1, -1], [1, 0], [-1, 0], [-1, 1], [-1,-1]]
WIDTH=16
WHITETAR = [[0,0], [1,0], [2,0], [3,0], [4,0],
			[0,1], [1,1], [2,1], [3,1], [4,1], 
			[0,2], [1,2], [2,2], [3,2],
			[0,3], [1,3], [2,3],
			[0,4], [1,4]]
BLACKTAR = [                           [14,11], [15,11],
					 		  [13,12], [14,12], [15,12],
			         [12,13], [13,13], [14,13], [15,13],
			[11,14], [12,14], [13,14], [14,14], [15,14],
			[11,15], [12,15], [13,15], [14,15], [15,15]]
import math
import time

GAME_TEST = 1
MODE = 0
TIME = 0
COLOR = 0
BOARD = []
	
	

# find legal moves for a given piece
def find_legal_moves(board, x, y):
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	# print "   ",x,y,"   "
	legal_moves = []
	visited = []

	# in target camp
	tarcamp = WHITETAR if board[y][x] == WHITE else BLACKTAR
	if any((i[0]==x and i[1]==y) for i in tarcamp):
		return legal_moves

	# no jump
	for p, q in neighbors:
		newx, newy = x+p, y+q
		if newx>=0 and newy>=0 and newx<WIDTH and newy<WIDTH and board[newy][newx]=='.':
			legal_moves.append([x, y, newx, newy])

	#jumps
	jumps = [[x,y]] #[x, y, x1, y1]
	while len(jumps)>0:
		cur = jumps.pop()
		cx, cy = cur[-2],cur[-1]
		for p, q in neighbors:
			newx, newy = cx+p, cy+q
			# jumped to visited loc
			if any((cx+p*2==i[0] and cy+q*2==i[1]) for i in visited):
				continue
			# jumped to unvisited loc
			if newx>=0 and newy>=0 and newx<WIDTH and newy<WIDTH and board[newy][newx]!='.':
				newx, newy = cx+p*2, cy+q*2
				if newx>=0 and newy>=0 and newx<WIDTH and newy<WIDTH and board[newy][newx]=='.':
					new_jump = cur + [newx, newy]
					visited.append([newx, newy])
					jumps.append(new_jump) 
					legal_moves.append(new_jump)
	# print legal_moves
	return legal_moves

def eval(state, color):
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	white = 0.0
	black = 0.0
	for x in range(WIDTH):
		for y in range(WIDTH):
			if state[y][x] == WHITE:
				incamp = any((i[0]==x and i[1]==y) for i in WHITETAR)
				d = math.sqrt((0-x)**2+(0-y)**2)
				a = 10.0 if d == 0.0 else 1/d
				white += a + 10.0*incamp
			if state[y][x] == BLACK:
				incamp = any((i[0]==x and i[1]==y) for i in BLACKTAR)
				d = math.sqrt((15-x)**2+(15-y)**2)
				a = 10.0 if d == 0.0 else 1/d
				black += a + 10.0*incamp
	# for line in state:
	# 	print line
	return white if COLOR == WHITE else black


def end_game(board, color):
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	tarcamp = WHITETAR if color == WHITE else BLACKTAR
	return all(board[y][x] == color for x,y in tarcamp)

def minimax(state, depth, color):
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	best = [-1, -1, -1,-1,-float("inf")] if color == COLOR else [-1, -1, -1,-1,float("inf")]
	opponent = WHITE if color == BLACK else BLACK
	if depth == 0 or end_game(state, opponent):
		score = eval(state, COLOR)

		return [-1,-1,-1,-1,score]

	# find all my pieces
	my_pieces = []
	camp = BLACKTAR if color == WHITE else WHITETAR
	incamp = any((state[i[1]][i[0]]==color) for i in camp)
	tarcamp = WHITETAR if color == WHITE else BLACKTAR

	if incamp: # incamp, need to move away
		for x,y in camp:
			if state[y][x] == color:
				my_pieces.append([x,y])
	else:
		
		for x in range(WIDTH):
			for y in range(WIDTH):
				if not any((x==i[0] and y==i[1]) for i in tarcamp) and BOARD[y][x] == COLOR:
					my_pieces.append([x,y])

	# find all legal moves
	all_legal_moves = [] # [cx, cy, tx, ty]
	for piece in my_pieces:
		moves = find_legal_moves(state, piece[0], piece[1])
		if len(moves) == 0:
			continue
		all_legal_moves += moves 
	# print "-------------"
	# generate new states
	for move in all_legal_moves:
		piece = state[move[1]][move[0]]
		state[move[1]][move[0]] = '.'
		state[move[-1]][move[-2]] = piece
		# for line in state:
		# 	print line
		score = minimax(state, depth-1, opponent)
		
		# print move, score[4], best

		state[move[1]][move[0]] = piece
		state[move[-1]][move[-2]] = '.'
		score[0:-1] = move
		if color == COLOR:
			if score[-1] > best[-1]:
				best = score # max
		else:
			if score[-1] < best[-1]:
				best = score # min
	# if color == COLOR :
	# 	print "max"
	# else:
	# 	print "min"
	return best

def main():
	
	global COLOR, WEVALMAP, BEVALMAP, MODE, TIME, BOARD
	# read file
	f = open("output.txt")
	data = []
	for line in f.readlines():
		data.append(line)
	MODE = SINGLE if data[0].strip()=="SINGLE" else GAME
	COLOR = BLACK if data[1].strip()=="BLACK" else WHITE
	TIME = float(data[2].strip())
	BOARD = [list(i.strip()) for i in data[3:]]

	# TODO: time exceed
	# single step
	if MODE == SINGLE:
		depth = 1
		move = minimax(BOARD, depth, COLOR)
		# print move
	
		# output
		f = open("output.txt", "w")
		if len(move)>5 or abs(move[0]-move[2])>1 or abs(move[1]-move[3])>1: 
			#jump
			for i in range(0,len(move)-3, 2):
				ret = "J %d,%d %d,%d" % (move[0+i], move[1+i], move[2+i], move[3+i])
				print ret
				f.write(ret)
				if i+2<len(move)-3:
					f.write("\n")
		else:
			#walk
			print "E %d,%d %d,%d" % (move[0], move[1], move[2], move[3])
			f.write("E %d,%d %d,%d" % (move[0], move[1], move[2], move[3]))
	
	
		# make the move
		piece = BOARD[move[1]][move[0]]
		BOARD[move[1]][move[0]] = '.'
		BOARD[move[-2]][move[-3]] = piece
		# for line in BOARD:
		# 	print line
	
	
	
	# game agent
	if MODE == GAME:
		depth = 1
		move = minimax(BOARD, depth, COLOR)
		print move
	
		# output
		f = open("output.txt", "w")
		if len(move)>5 or abs(move[0]-move[2])>1 or abs(move[1]-move[3])>1: 
			#jump
			for i in range(0,len(move)-3, 2):
				ret = "J %d,%d %d,%d" % (move[0+i], move[1+i], move[2+i], move[3+i])
				print ret
				f.write(ret)
				if i+2<len(move)-3:
					f.write("\n")
	
		else:
			#walk
			print "E %d,%d %d,%d" % (move[0], move[1], move[2], move[3])
			f.write("E %d,%d %d,%d" % (move[0], move[1], move[2], move[3]))
		f.close()
	
	
		# make the move
		piece = BOARD[move[1]][move[0]]
		BOARD[move[1]][move[0]] = '.'
		BOARD[move[-2]][move[-3]] = piece
		for line in BOARD:
			print line
	
		
		if GAME_TEST:
			f = open("input.txt", "w")
			f.write("GAME\n")
			f.write("WHITE\n")
			f.write(str(TIME)+"\n")
			count = 0
			for line in BOARD:
				f.write("".join(line))
				count+=1
				if count != 16:
					f.write("\n")
			f.close()

if __name__ == '__main__':
	
	
	main()

	
